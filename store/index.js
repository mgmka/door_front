export const state = () => ({
  host: ''
})

export const mutations = {
  set_host (state, hostname) {
    state.host = hostname
  }
}

export const actions = {
  async nuxtServerInit ({ commit }, { req }) {
    //commit('set_host', req.headers['host'])
    if (req.headers['host']) {
      commit('set_host', req.headers['host'])
    }
  }
}
